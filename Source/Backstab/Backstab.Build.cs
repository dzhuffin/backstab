using UnrealBuildTool;

public class Backstab : ModuleRules
{
	public Backstab(TargetInfo Target)
	{
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "AIModule" });
	}
}
