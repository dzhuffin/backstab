// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Animation/AnimInstance.h"
#include "BackstabAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class BACKSTAB_API UBackstabAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	
	//------------------------------------------------------------------------------------------------
	UBackstabAnimInstance(const FObjectInitializer& ObjectInitializer);


	//------------------------------------------------------------------------------------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float Speed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	bool StartMovementTrigger;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	bool StopMovementTrigger;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float StartAngle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	float ArchAngle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement, meta = (ToolTip = "[0.0; 1.0] - 1.0f if right leg ahead, 0.0f if left leg ahead"))
	float RightLegProgress;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	bool BreakForTurn;

};
