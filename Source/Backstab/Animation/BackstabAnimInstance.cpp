// Fill out your copyright notice in the Description page of Project Settings.

#include "Backstab.h"
#include "BackstabAnimInstance.h"


//------------------------------------------------------------------------------------------------
UBackstabAnimInstance::UBackstabAnimInstance(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	Speed = 0.0f;
	StartAngle = 0.0f;
	ArchAngle = 0.0f;
	BreakForTurn = false;
}

