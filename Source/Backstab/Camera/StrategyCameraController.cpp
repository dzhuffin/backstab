// Fill out your copyright notice in the Description page of Project Settings.

#include "Backstab.h"
#include "BackstabCharacter.h"
#include "Engine/LocalPlayer.h"
#include "StrategyCameraController.h"
#include "AI/PlayableCharacterController.h"
#include "HUD/MainHUD.h"


//------------------------------------------------------------------------------------------------
AStrategyCameraController::AStrategyCameraController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;

	bAutoManageActiveCameraTarget = true;
}


//------------------------------------------------------------------------------------------------
void AStrategyCameraController::SetupInputComponent()
{
	_outlinerUpdateTimer = 0.0f;

	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &AStrategyCameraController::OnSetDestination);
	InputComponent->BindAction("Select", IE_Pressed, this, &AStrategyCameraController::OnStartSelect);
	InputComponent->BindAction("Select", IE_Released, this, &AStrategyCameraController::OnFinishSelect);

	InputComponent->BindAction("PoseToStand", IE_Pressed, this, &AStrategyCameraController::OnChangePoseToStand);
	InputComponent->BindAction("PoseToSit", IE_Pressed, this, &AStrategyCameraController::OnChangePoseToSit);
	InputComponent->BindAction("PoseToLie", IE_Pressed, this, &AStrategyCameraController::OnChangePoseToLie);

	InputComponent->BindAxis("MoveForward", this, &AStrategyCameraController::OnXAxis);
	InputComponent->BindAxis("MoveRight", this, &AStrategyCameraController::OnYAxis);
}


//------------------------------------------------------------------------------------------------
void AStrategyCameraController::Possess(APawn* PossessedPawn)
{
	Super::Possess(PossessedPawn);

	_cameraPawn = Cast<AStrategyCameraPawn>(PossessedPawn);
}


//------------------------------------------------------------------------------------------------
void AStrategyCameraController::PlayerTick(float dt)
{
	Super::PlayerTick(dt);

	_outlinerUpdateTimer += dt;
	
	AMainHUD* const mainHUD = Cast<AMainHUD>(GetHUD());
	if (mainHUD != nullptr && mainHUD->GetSelectionFrameActive())
	{
		// update selection frame

		ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
		bool bHit = false;
		if (LocalPlayer)
		{
			FVector2D MousePosition;
			if (LocalPlayer->ViewportClient->GetMousePosition(MousePosition))
			{
				mainHUD->OnUpdateSelectionFrame(MousePosition);
			}
		}

		if (_outlinerUpdateTimer > OutlinerUpdatePeriod)
		{
			GetGameMode()->PreSelectCharacters(mainHUD->GetSelectedCharactersIterator());

			_outlinerUpdateTimer = 0.0f;
		}
	}
	else
	{
		// pre-select characters

		if (_outlinerUpdateTimer > OutlinerUpdatePeriod)
		{
			_mouseOverHit.bBlockingHit = false;
			GetHitResultUnderCursor(ECC_Pawn, false, _mouseOverHit);

			APlayableCharacterController* characterController = nullptr;
			if (_mouseOverHit.bBlockingHit)
			{
				ACharacter* character = Cast<ACharacter>(_mouseOverHit.GetActor());
				if (character)
					characterController = Cast<APlayableCharacterController>(character->GetController());
			}

			GetGameMode()->PreSelectCharacter(characterController);

			_outlinerUpdateTimer = 0.0f;
		}
	}
}


//------------------------------------------------------------------------------------------------
void AStrategyCameraController::OnXAxis(float value)
{
	GetPawn()->AddMovementInput(FVector(value, 0.0f, 0.0f));
}


//------------------------------------------------------------------------------------------------
void AStrategyCameraController::OnYAxis(float value)
{
	GetPawn()->AddMovementInput(FVector(0.0f, value, 0.0f));
}


//------------------------------------------------------------------------------------------------
float AStrategyCameraController::GetCameraZoom()
{
	return GetStrategyCameraPawn()->GetCameraBoom()->TargetArmLength;
}


//------------------------------------------------------------------------------------------------
void AStrategyCameraController::SetCameraZoom(float zoom)
{
	GetStrategyCameraPawn()->GetCameraBoom()->TargetArmLength = zoom;
}


//------------------------------------------------------------------------------------------------
void AStrategyCameraController::ChangePose_Internal(ECharacterPose::Pose newPose)
{
	for (auto it = GetGameMode()->GetCharacterControllersIterator(); it; ++it)
	{
		APlayableCharacterController* currentCharacterController = *it;
		if (currentCharacterController != nullptr)
		{
			ABackstabCharacter* character = Cast<ABackstabCharacter>(currentCharacterController->GetPawn());
			if (character != nullptr && character->IsSelected())
				character->ChangePose(newPose);
		}
	}
}


//------------------------------------------------------------------------------------------------
void AStrategyCameraController::OnChangePoseToStand()
{
	ChangePose_Internal(ECharacterPose::CP_Stand);
}


//------------------------------------------------------------------------------------------------
void AStrategyCameraController::OnChangePoseToSit()
{
	ChangePose_Internal(ECharacterPose::CP_Sit);
}


//------------------------------------------------------------------------------------------------
void AStrategyCameraController::OnChangePoseToLie()
{
	ChangePose_Internal(ECharacterPose::CP_Lie);
}


//------------------------------------------------------------------------------------------------
void AStrategyCameraController::OnSetDestination()
{
	_mouseSelectHit.bBlockingHit = false;

	GetHitResultUnderCursor(ECC_Visibility, false, _mouseSelectHit);

	if (_mouseSelectHit.bBlockingHit)
	{
		for (auto it = GetGameMode()->GetCharacterControllersIterator(); it; ++it)
		{
			APlayableCharacterController* currentCharacterController = *it;
			if (currentCharacterController != nullptr)
			{
				ABackstabCharacter* character = Cast<ABackstabCharacter>(currentCharacterController->GetPawn());
				if (character != nullptr && character->IsSelected())
					currentCharacterController->SetNewMoveDestination(_mouseSelectHit.ImpactPoint);
			}
		}
	}
}


//------------------------------------------------------------------------------------------------
void AStrategyCameraController::OnStartSelect()
{
	AMainHUD* const mainHUD = Cast<AMainHUD>(GetHUD());
	if (mainHUD != nullptr)
	{
		ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
		bool bHit = false;
		if (LocalPlayer)
		{
			FVector2D MousePosition;
			if (LocalPlayer->ViewportClient->GetMousePosition(MousePosition))
			{
				// start selection frame
				mainHUD->OnStartSelectionFrame(MousePosition);

				// select character logic
				_mouseSelectHit.bBlockingHit = false;
				GetHitResultAtScreenPosition(MousePosition, ECC_Pawn, false, _mouseSelectHit);
				if (_mouseSelectHit.bBlockingHit)
				{
					ACharacter* character = Cast<ACharacter>(_mouseSelectHit.GetActor());
					if (character)
					{
						APlayableCharacterController* characterController = Cast<APlayableCharacterController>(character->GetController());
						if (characterController)
							GetGameMode()->SelectCharacter(characterController);
					}
				}
			}
		}
	}
}


//------------------------------------------------------------------------------------------------
void AStrategyCameraController::OnFinishSelect()
{
	AMainHUD* const mainHUD = Cast<AMainHUD>(GetHUD());
	if (mainHUD != nullptr)
		mainHUD->OnFinishSelectionFrame();

	GetGameMode()->SelectCharacters(mainHUD->GetSelectedCharactersIterator());
}