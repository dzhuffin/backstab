#pragma once

#include "GameFramework/SpectatorPawn.h"
#include "StrategyCameraPawn.generated.h"

/**
 * 
 */
UCLASS()
class BACKSTAB_API AStrategyCameraPawn : public ASpectatorPawn
{
	GENERATED_BODY()


	//------------------------------------------------------------------------------------------------
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
	

	//------------------------------------------------------------------------------------------------
	virtual void BeginPlay() override;


public:

	//------------------------------------------------------------------------------------------------
	AStrategyCameraPawn(const FObjectInitializer& ObjectInitializer);


	//------------------------------------------------------------------------------------------------
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
};
