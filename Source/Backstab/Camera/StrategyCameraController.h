// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BackstabGameMode.h"
#include "Common/CommonPlayerController.h"
#include "StrategyCameraController.generated.h"

/**
 * 
 */
UCLASS()
class BACKSTAB_API AStrategyCameraController : public ACommonPlayerController
{
	GENERATED_BODY()

public:

	//------------------------------------------------------------------------------------------------
	AStrategyCameraController(const FObjectInitializer& ObjectInitializer);

private:

	//------------------------------------------------------------------------------------------------
	const float OutlinerUpdatePeriod = 0.1f;


	//------------------------------------------------------------------------------------------------
	float _outlinerUpdateTimer;	


	//------------------------------------------------------------------------------------------------
	AStrategyCameraPawn* _cameraPawn;


	//------------------------------------------------------------------------------------------------
	FHitResult _mouseSelectHit;
	FHitResult _mouseOverHit;

	//------------------------------------------------------------------------------------------------
	void OnXAxis(float value);
	void OnYAxis(float value);
	void OnSetDestination();
	void OnStartSelect();
	void OnFinishSelect();
	void OnChangePoseToStand();
	void OnChangePoseToSit();
	void OnChangePoseToLie();


	//------------------------------------------------------------------------------------------------
	void ChangePose_Internal(ECharacterPose::Pose newPose);


protected:

	//------------------------------------------------------------------------------------------------
	virtual void PlayerTick(float dt) override;
	virtual void SetupInputComponent() override;


public:

	//------------------------------------------------------------------------------------------------
	float GetCameraZoom();
	void SetCameraZoom(float zoom);


	//------------------------------------------------------------------------------------------------
	virtual void Possess(APawn* PossessedPawn) override;


	//------------------------------------------------------------------------------------------------
	FORCEINLINE class AStrategyCameraPawn* GetStrategyCameraPawn() const { return _cameraPawn; }
	
};
