// Fill out your copyright notice in the Description page of Project Settings.

#include "Backstab.h"
#include "BackstabCharacter.h"
#include "MainHUD.h"


//------------------------------------------------------------------------------------------------
AMainHUD::AMainHUD(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	_selectionFrameActive = false;
}


//------------------------------------------------------------------------------------------------
void AMainHUD::DrawHUD()
{
	Super::DrawHUD();

	if (_selectionFrameActive)
	{
		if (SelectionFrameMaterial != nullptr)
		{
			int x = (int)FMath::Min(_selectionFrameStart.X, _selectionFrameEnd.X);
			int y = (int)FMath::Min(_selectionFrameStart.Y, _selectionFrameEnd.Y);
			int width = (int)FMath::Abs(_selectionFrameEnd.X - _selectionFrameStart.X);
			int height = (int)FMath::Abs(_selectionFrameEnd.Y - _selectionFrameStart.Y);

			DrawMaterial(SelectionFrameMaterial, x, y, width, height, 0.0f, 0.0f, 1.0f, 1.0f);
		}

		_selectedCharacters.Empty();
		GetActorsInSelectionRectangle<ABackstabCharacter>(_selectionFrameStart, _selectionFrameEnd, _selectedCharacters, false, false);
	}
}


//------------------------------------------------------------------------------------------------
void AMainHUD::OnStartSelectionFrame(FVector2D startPoint)
{
	_selectionFrameStart = startPoint;
	_selectionFrameActive = true;
}


//------------------------------------------------------------------------------------------------
void AMainHUD::OnUpdateSelectionFrame(FVector2D endPoint)
{
	if (!_selectionFrameActive)
		return;

	_selectionFrameEnd = endPoint;
}


//------------------------------------------------------------------------------------------------
void AMainHUD::OnFinishSelectionFrame()
{
	_selectionFrameActive = false;
}