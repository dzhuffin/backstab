// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "MainHUD.generated.h"

/**
 * 
 */
UCLASS()
class BACKSTAB_API AMainHUD : public AHUD
{
	GENERATED_BODY()

	//------------------------------------------------------------------------------------------------
	UPROPERTY(EditAnywhere, Category = Default)
	class UMaterialInstance* SelectionFrameMaterial;

	
	//------------------------------------------------------------------------------------------------
	virtual void DrawHUD() override;

public:

	//------------------------------------------------------------------------------------------------
	AMainHUD(const FObjectInitializer& ObjectInitializer);

private:
	
	//------------------------------------------------------------------------------------------------
	bool _selectionFrameActive;
	FVector2D _selectionFrameStart;
	FVector2D _selectionFrameEnd;
	TArray<ABackstabCharacter*> _selectedCharacters;


public:

	//------------------------------------------------------------------------------------------------
	void OnStartSelectionFrame(FVector2D startPoint);
	void OnUpdateSelectionFrame(FVector2D endPoint);
	void OnFinishSelectionFrame();


	//------------------------------------------------------------------------------------------------
	FORCEINLINE bool GetSelectionFrameActive() const { return _selectionFrameActive; }
	FORCEINLINE TArray<ABackstabCharacter*>::TConstIterator GetSelectedCharactersIterator() const { return _selectedCharacters.CreateConstIterator(); }
};