// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "FollowerAIController.generated.h"

/**
 * 
 */
UCLASS()
class BACKSTAB_API AFollowerAIController : public AAIController
{
	GENERATED_BODY()


	//------------------------------------------------------------------------------------------------
	UPROPERTY(EditAnywhere, Category = AI)
	class UBehaviorTree* BehaviorTree;


protected:

	//------------------------------------------------------------------------------------------------
	FBlackboard::FKey HomeLocationKey;
	FBlackboard::FKey TargetLocationKey;
	FBlackboard::FKey TargetToFollowKey;
	FBlackboard::FKey SelfActorKey;


public:

	//------------------------------------------------------------------------------------------------
	virtual void Possess(APawn* PossessedPawn) override;
	virtual void UnPossess() override;

	//------------------------------------------------------------------------------------------------
	void SetTargetToFollow(APawn* newTarget);
};
