// Fill out your copyright notice in the Description page of Project Settings.

#include "Backstab.h"
#include "FollowerAIController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "BrainComponent.h"


//------------------------------------------------------------------------------------------------
void AFollowerAIController::Possess(APawn* PossessedPawn)
{
	Super::Possess(PossessedPawn);

	if (BehaviorTree != nullptr)
	{
		RunBehaviorTree(BehaviorTree);

		HomeLocationKey = Blackboard->GetKeyID("HomeLocation");
		TargetLocationKey = Blackboard->GetKeyID("TargetLocation");
		TargetToFollowKey = Blackboard->GetKeyID("TargetToFollow");
		SelfActorKey = Blackboard->GetKeyID("SelfActor");

		BrainComponent->GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(HomeLocationKey, GetPawn()->GetActorLocation());
	}
}


//------------------------------------------------------------------------------------------------
void AFollowerAIController::UnPossess()
{
	Super::UnPossess();

	UBehaviorTreeComponent* BTComp = Cast<UBehaviorTreeComponent>(BrainComponent);
	if (BTComp != nullptr)
		BTComp->StopTree();
}


//------------------------------------------------------------------------------------------------
void AFollowerAIController::SetTargetToFollow(APawn* newTarget)
{
	BrainComponent->GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(TargetToFollowKey, newTarget);
	BrainComponent->GetBlackboardComponent()->SetValue<UBlackboardKeyType_Vector>(TargetLocationKey, newTarget->GetActorLocation());
}