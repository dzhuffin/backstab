// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BackstabGameMode.h"
#include "AIController.h"
#include "PlayableCharacterController.generated.h"


/**
 * 
 */
UCLASS()
class BACKSTAB_API APlayableCharacterController : public AAIController
{
	GENERATED_BODY()

public:

	//------------------------------------------------------------------------------------------------
	APlayableCharacterController(const FObjectInitializer& ObjectInitializer);


private:

	//------------------------------------------------------------------------------------------------
	ABackstabGameMode *_gameMode;
	ABackstabCharacter* _character;

	FHitResult _mouseMoveHit;
	FHitResult _touchMoveHit;

	
public:

	//------------------------------------------------------------------------------------------------
	float GetCameraZoom();
	void SetCameraZoom(float zoom);


	//------------------------------------------------------------------------------------------------
	float CalculateAngleToFocalPoint() const;
	void SetNewMoveDestination(const FVector destLocation);
	void OnSelected(bool selected);


	//------------------------------------------------------------------------------------------------
	virtual void Tick(float deltaTime) override;
	virtual void Possess(APawn* PossessedPawn) override;
	//virtual void UpdateControlRotation(float deltaTime, bool updatePawn = false) override;


	//------------------------------------------------------------------------------------------------
	FORCEINLINE class ABackstabCharacter* GetBackstabCharacter() const { return _character; }
	
};
