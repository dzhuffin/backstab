#pragma once

#include "GameFramework/Character.h"
#include "AiBotCharacter.generated.h"

UCLASS()
class BACKSTAB_API AAiBotCharacter : public ACharacter
{
	GENERATED_BODY()

	//------------------------------------------------------------------------------------------------
	UPROPERTY(VisibleAnywhere, Category = AI)
	class UPawnSensingComponent* PawnSensingComponent;

protected:

	//------------------------------------------------------------------------------------------------
	UFUNCTION(BlueprintCallable, Category = AI)
	void OnSeePlayer(APawn* pawn);

	UFUNCTION(BlueprintCallable, Category = AI)
	void OnHearNoise(APawn* PawnInstigator, const FVector& Location, float Volume);

public:

	//------------------------------------------------------------------------------------------------
	AAiBotCharacter();

	//------------------------------------------------------------------------------------------------
	virtual void BeginPlay() override;
	virtual void Tick(float dt) override;
	virtual void PostInitializeComponents() override;
};
