// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Navigation/PathFollowingComponent.h"
#include "BackstabPathFollowingComponent.generated.h"

/**
 * 
 */
UCLASS()
class BACKSTAB_API UBackstabPathFollowingComponent : public UPathFollowingComponent
{
	GENERATED_BODY()
	
	//------------------------------------------------------------------------------------------------
	ABackstabCharacter* _character;
	APlayableCharacterController* _controller;

	//------------------------------------------------------------------------------------------------
	virtual void Initialize();

protected:

	//------------------------------------------------------------------------------------------------
	virtual void OnPathFinished(const FPathFollowingResult& result) override;
	virtual void FollowPathSegment(float deltaTime) override;
	
};
