// Fill out your copyright notice in the Description page of Project Settings.

#include "Backstab.h"
#include "AIController.h"
#include "BackstabCharacter.h"
#include "PlayableCharacterController.h"
#include "BackstabPathFollowingComponent.h"


//------------------------------------------------------------------------------------------------
void UBackstabPathFollowingComponent::Initialize()
{
	Super::Initialize();

	_controller = Cast<APlayableCharacterController>(GetOwner());
	if (_controller != nullptr)
	{
		_character = Cast<ABackstabCharacter>(_controller->GetCharacter());
	}
}


//------------------------------------------------------------------------------------------------
void UBackstabPathFollowingComponent::OnPathFinished(const FPathFollowingResult& result)
{
	Super::OnPathFinished(result);

	if (_character != nullptr)
		_character->StopMovement();
}


//------------------------------------------------------------------------------------------------
void UBackstabPathFollowingComponent::FollowPathSegment(float deltaTime)
{
	if (_character != nullptr)
	{
		if (_character->IsInMovementLoop)
		{
			float rotAngle = _controller->CalculateAngleToFocalPoint();

			if (FMath::Abs(rotAngle) <= _character->NonBreakMovementAngle)
			{
				FVector dirToFocalPoint = _controller->GetFocalPoint() - _character->GetActorLocation();
				dirToFocalPoint.Z = 0.0f;
				dirToFocalPoint.Normalize();

				auto currentDir = FMath::Abs(rotAngle) > 5.0f ?
					FMath::Lerp(_character->GetActorForwardVector(), dirToFocalPoint, deltaTime * 10.0f) :
					dirToFocalPoint;

				FRotator newRot = FRotationMatrix::MakeFromX(currentDir).Rotator();

				_character->SetActorRotation(newRot);

				_controller->SetControlRotation(newRot);
			}
			else
			{
				_character->BreakMovementForTurn(rotAngle);
			}
		}
	}
}



