#include "Backstab.h"
#include "AiBotCharacter.h"
#include "BackstabCharacter.h"
#include "AI/FollowerAIController.h"
#include "Perception/PawnSensingComponent.h"


//------------------------------------------------------------------------------------------------
AAiBotCharacter::AAiBotCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	PawnSensingComponent = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComponent"));
	PawnSensingComponent->SetPeripheralVisionAngle(60.0f);
	PawnSensingComponent->SightRadius = 2000;
	PawnSensingComponent->HearingThreshold = 600;
	PawnSensingComponent->LOSHearingThreshold = 1200;
	PawnSensingComponent->bOnlySensePlayers = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	GetCharacterMovement()->GetNavAgentPropertiesRef().bCanCrouch = true;
}


//------------------------------------------------------------------------------------------------
void AAiBotCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}


//------------------------------------------------------------------------------------------------
void AAiBotCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (PawnSensingComponent != nullptr)
	{
		PawnSensingComponent->OnSeePawn.AddDynamic(this, &AAiBotCharacter::OnSeePlayer);
		PawnSensingComponent->OnHearNoise.AddDynamic(this, &AAiBotCharacter::OnHearNoise);
	}
}


//------------------------------------------------------------------------------------------------
void AAiBotCharacter::Tick(float dt)
{
	Super::Tick(dt);

}


//------------------------------------------------------------------------------------------------
void AAiBotCharacter::OnSeePlayer(APawn* pawn)
{
	ABackstabCharacter* character = Cast<ABackstabCharacter>(pawn);
	if (character == nullptr)
		return;

	//GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, pawn->GetName());

	AFollowerAIController* controller = Cast<AFollowerAIController>(GetController());
	if (controller != nullptr)
		controller->SetTargetToFollow(pawn);
}


//------------------------------------------------------------------------------------------------
void AAiBotCharacter::OnHearNoise(APawn* PawnInstigator, const FVector& Location, float Volume)
{
}