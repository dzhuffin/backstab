// Fill out your copyright notice in the Description page of Project Settings.

#include "Backstab.h"
#include "BackstabCharacter.h"
#include "PlayableCharacterController.h"
#include "AI/BackstabPathFollowingComponent.h"


//------------------------------------------------------------------------------------------------
APlayableCharacterController::APlayableCharacterController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UBackstabPathFollowingComponent>(TEXT("PathFollowingComponent")))
{
	PrimaryActorTick.bCanEverTick = true;
}


//------------------------------------------------------------------------------------------------s
float APlayableCharacterController::CalculateAngleToFocalPoint() const
{
	FVector fwdVector = _character->GetActorForwardVector();
	FVector aiDir = GetFocalPoint() - _character->GetActorLocation();

	float angle = FMath::RadiansToDegrees(FMath::Acos(fwdVector.CosineAngle2D(aiDir)));
	if (FVector::CrossProduct(fwdVector, aiDir).Z < 0.0f)
		angle *= -1.0f;

	return angle;
}


//------------------------------------------------------------------------------------------------
void APlayableCharacterController::SetNewMoveDestination(const FVector destLocation)
{
	UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
	float const distance = FVector::Dist(destLocation, _character->GetActorLocation());

	if (NavSys != nullptr && (distance > 120.0f))
	{
		NavSys->SimpleMoveToLocation(this, destLocation);

		float angle = CalculateAngleToFocalPoint();

		if (FMath::Abs(angle) >= _character->NonBreakMovementAngle || !_character->IsInMovementLoop)
			_character->BreakMovementForTurn(angle);

		_character->StartMovement(1.0f);
	}
}


//------------------------------------------------------------------------------------------------
void APlayableCharacterController::Tick(float deltaTime)
{
	DrawDebugSphere(GetWorld(), GetFocalPoint(), 32, 32, FColor::Red);
}


//------------------------------------------------------------------------------------------------
//void APlayableCharacterController::UpdateControlRotation(float deltaTime, bool updatePawn)
//{
//	if (_character->IsInMovementLoop)
//	{
//		float rotAngle = CalculateAngleToFocalPoint();
//
//		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Green, FString::SanitizeFloat(rotAngle));
//
//		if (FMath::Abs(rotAngle) <= _character->NonBreakMovementAngle)
//		{
//			//_character->SetAnimArchAngle(rotAngle / _character->NonBreakMovementAngle * 180.0f);
//
//			FQuat newPawnRotation = FQuat::MakeFromEuler(FVector(0.0f, 0.0f, rotAngle));
//
//			FTransform pawnTransform = _character->GetTransform();
//			pawnTransform.SetRotation(newPawnRotation);
//
//			_character->GetCapsuleComponent()->SetWorldTransform(pawnTransform);
//
//			SetControlRotation(pawnTransform.Rotator());
//		}
//		else
//		{
//			//_character->SetAnimArchAngle(0.0f);
//			_character->SetAnimBreakForTurn(true);
//			_character->SetAnimStartAngle(rotAngle);
//		}
//	}
//}


//------------------------------------------------------------------------------------------------
void APlayableCharacterController::Possess(APawn* PossessedPawn)
{
	Super::Possess(PossessedPawn);

	_gameMode = Cast<ABackstabGameMode>(UGameplayStatics::GetGameMode(this));
	_character = Cast<ABackstabCharacter>(PossessedPawn);
}


//------------------------------------------------------------------------------------------------
void APlayableCharacterController::OnSelected(bool selected)
{
	_character->OnSelectorChanged(selected);
}


//------------------------------------------------------------------------------------------------
float APlayableCharacterController::GetCameraZoom()
{
	return _character->GetCameraBoom()->TargetArmLength;
}


//------------------------------------------------------------------------------------------------
void APlayableCharacterController::SetCameraZoom(float zoom)
{
	_character->GetCameraBoom()->TargetArmLength = zoom;
}