#pragma once
#include "GameFramework/Character.h"
#include "BackstabCharacter.generated.h"


UENUM(BlueprintType)
namespace ECharacterPose
{
	enum Pose
	{
		CP_Stand        UMETA(DisplayName = "Stand"),
		CP_Sit          UMETA(DisplayName = "Sit"),
		CP_Lie          UMETA(DisplayName = "Lie"),

		CM_Max          UMETA(Hidden),
	};
}


class UBackstabAnimInstance;


UCLASS(Blueprintable)
class ABackstabCharacter : public ACharacter
{
	GENERATED_BODY()

	//------------------------------------------------------------------------------------------------

	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Hud, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* SelectorMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Hud, meta = (AllowPrivateAccess = "true"))
	bool SelectorVisible;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	TEnumAsByte<ECharacterPose::Pose> BodyPose;


	//------------------------------------------------------------------------------------------------
	virtual void BeginPlay() override;

private:

	//------------------------------------------------------------------------------------------------
	UBackstabAnimInstance* _animInstance;


public:

	
	//------------------------------------------------------------------------------------------------s
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Animation, meta = (AllowPrivateAccess = "true"))
	bool IsInMovementLoop;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	float NonBreakMovementAngle;


	//------------------------------------------------------------------------------------------------
	ABackstabCharacter(const FObjectInitializer& ObjectInitializer);


	//------------------------------------------------------------------------------------------------
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	FORCEINLINE bool IsSelected() const { return SelectorVisible; }


	//------------------------------------------------------------------------------------------------
	void OnSelectorChanged(bool selected);
	void ShowPreselection(bool visible);
	void ChangePose(ECharacterPose::Pose newPose);

	//------------------------------------------------------------------------------------------------
	void SetAnimSpeed(float newValue);
	void SetAnimArchAngle(float newValue);
	void StartMovement(float speed);
	void BreakMovementForTurn(float turnAngle);
	void StopMovement();
};

