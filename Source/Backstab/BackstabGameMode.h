#pragma once

#include "BackstabCharacter.h"
#include "Camera/StrategyCameraPawn.h"
#include "GameFramework/GameMode.h"
#include "BackstabGameMode.generated.h"

class AStrategyCameraController;
class APlayableCharacterController;

UENUM(BlueprintType)
namespace ECameraMode
{
	enum Mode
	{
		CM_Strategy     UMETA(DisplayName = "Strategy"),
		CM_Follow       UMETA(DisplayName = "Follow"),

		CM_Max          UMETA(Hidden),
	};
}

UCLASS(minimalapi)
class ABackstabGameMode : public AGameMode
{
	GENERATED_BODY()

private:

	//------------------------------------------------------------------------------------------------
	int _currentCharacter;
	TEnumAsByte<ECameraMode::Mode> _cameraMode;
	TArray<APlayableCharacterController*> _characterControllers;
	AStrategyCameraPawn *_stategyCameraPawn;
	AStrategyCameraController* _strategyCameraController;


	//------------------------------------------------------------------------------------------------
	void SwitchCharacter_Internal(int newCharacterIndex);


	//------------------------------------------------------------------------------------------------
	virtual void StartPlay() override;
	virtual void Tick(float dt) override;

public:

	//------------------------------------------------------------------------------------------------
	ABackstabGameMode(const FObjectInitializer& ObjectInitializer);


	//------------------------------------------------------------------------------------------------
	UFUNCTION(BlueprintCallable, Category = Camera)
	float GetCameraZoom();

	UFUNCTION(BlueprintCallable, Category = Camera)
	void SetCameraZoom(float zoom);


	//------------------------------------------------------------------------------------------------
	void SwitchCameraMode();
	void SwitchCharacter();
	void SelectCharacter(APlayableCharacterController* newController);
	void SelectCharacters(TArray<ABackstabCharacter*>::TConstIterator characterIteratior);
	void PreSelectCharacter(APlayableCharacterController* newController);
	void PreSelectCharacters(TArray<ABackstabCharacter*>::TConstIterator characterIteratior);


	//------------------------------------------------------------------------------------------------
	FORCEINLINE class AStrategyCameraPawn* GetStrategyCameraPawn() const { return _stategyCameraPawn; }
	FORCEINLINE class AStrategyCameraController* GetStrategyCameraController() const { return _strategyCameraController; }
	FORCEINLINE class APlayableCharacterController* GetCurrentCharacterController() const { return _characterControllers[_currentCharacter]; }
	FORCEINLINE TArray<APlayableCharacterController*>::TConstIterator GetCharacterControllersIterator() const { return _characterControllers.CreateConstIterator(); }
};