#include "Backstab.h"
#include "BackstabGameMode.h"
#include "HUD/MainHUD.h"
#include "AI/PlayableCharacterController.h"
#include "Camera/StrategyCameraPawn.h"
#include "Camera/StrategyCameraController.h"


//------------------------------------------------------------------------------------------------
ABackstabGameMode::ABackstabGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;

	PlayerControllerClass = AStrategyCameraController::StaticClass();
	SpectatorClass = AStrategyCameraPawn::StaticClass();

	static ConstructorHelpers::FClassFinder<AMainHUD> MainHUDBPClass(TEXT("/Game/TopDown/UI/MainHUD_BP"));
	if (MainHUDBPClass.Class != nullptr)
	{
		HUDClass = MainHUDBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<APawn> CameraPawnBPClass(TEXT("/Game/TopDown/Blueprints/StrategyCameraPawn_BP"));
	if (CameraPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = CameraPawnBPClass.Class;
	}

	_currentCharacter = 0;
	_stategyCameraPawn = nullptr;
	_cameraMode = ECameraMode::CM_Strategy;
}


//------------------------------------------------------------------------------------------------
void ABackstabGameMode::StartPlay()
{
	Super::StartPlay();

	auto const world = GetWorld();
	if (world)
	{
		TArray<AActor*> outActors;
		UGameplayStatics::GetAllActorsOfClass(this, ABackstabCharacter::StaticClass(), outActors);

		// gather characters controllers
		bool isSelected = false;
		for (auto actor : outActors)
		{
			auto character = Cast<ABackstabCharacter>(actor);
			if (character)
			{
				auto characterController = Cast<APlayableCharacterController>(character->GetController());
				
				if (characterController)
				{
					characterController->OnSelected(!isSelected);
					isSelected = true;

					_characterControllers.Add(characterController);
				}
			}
		}
	}

	_strategyCameraController = Cast<AStrategyCameraController>(UGameplayStatics::GetPlayerController(this, 0));

	if (_cameraMode == ECameraMode::CM_Strategy)
		SwitchCameraMode();
}


//------------------------------------------------------------------------------------------------
void ABackstabGameMode::Tick(float dt)
{
	Super::Tick(dt);
}


//------------------------------------------------------------------------------------------------
void ABackstabGameMode::SwitchCharacter_Internal(int newCharacterIndex)
{
	GetCurrentCharacterController()->OnSelected(false);

	_currentCharacter = newCharacterIndex;

	GetCurrentCharacterController()->OnSelected(true);
}


//------------------------------------------------------------------------------------------------
void ABackstabGameMode::SwitchCharacter()
{
	if (_characterControllers.Num() <= 0)
		return;

	int newCharacterIndex = _currentCharacter + 1;
	if (newCharacterIndex >= _characterControllers.Num())
		newCharacterIndex = 0;

	SwitchCharacter_Internal(newCharacterIndex);
}


//------------------------------------------------------------------------------------------------
void ABackstabGameMode::SelectCharacter(APlayableCharacterController* newController)
{
	for (int i = 0; i < _characterControllers.Num(); i++)
	{
		bool selected = _characterControllers[i] == newController;
		if (selected)
			_currentCharacter = i;

		_characterControllers[i]->OnSelected(selected);
	}
}


//------------------------------------------------------------------------------------------------
void ABackstabGameMode::SelectCharacters(TArray<ABackstabCharacter*>::TConstIterator characterIteratior)
{
	ABackstabCharacter* currentCharacter = nullptr;
	ABackstabCharacter* selectedCharacter = nullptr;

	for (int i = 0; i < _characterControllers.Num(); i++)
	{
		bool found = false;
		currentCharacter = Cast<ABackstabCharacter>(_characterControllers[i]->GetCharacter());

		for (auto charactersIt = characterIteratior; charactersIt; charactersIt++)
		{
			selectedCharacter = Cast<ABackstabCharacter>(*charactersIt);

			if (currentCharacter != nullptr && currentCharacter == selectedCharacter)
			{
				found = true;
				break;
			}
		}

		characterIteratior.Reset();

		if (currentCharacter != nullptr)
		{
			APlayableCharacterController* const controller = Cast<APlayableCharacterController>(currentCharacter->GetController());
			if (controller != nullptr)
				controller->OnSelected(found);
		}
	}
}


//------------------------------------------------------------------------------------------------
void ABackstabGameMode::PreSelectCharacter(APlayableCharacterController* newController)
{
	for (int i = 0; i < _characterControllers.Num(); i++)
	{
		ABackstabCharacter* const character = Cast<ABackstabCharacter>(_characterControllers[i]->GetCharacter());

		if (character != nullptr)
			character->ShowPreselection(_characterControllers[i] == newController);
	}
}


//------------------------------------------------------------------------------------------------
void ABackstabGameMode::PreSelectCharacters(TArray<ABackstabCharacter*>::TConstIterator characterIteratior)
{
	ABackstabCharacter* currentCharacter = nullptr;
	ABackstabCharacter* selectedCharacter = nullptr;

	for (int i = 0; i < _characterControllers.Num(); i++)
	{
		bool found = false;
		currentCharacter = Cast<ABackstabCharacter>(_characterControllers[i]->GetCharacter());

		for (auto charactersIt = characterIteratior; charactersIt; charactersIt++)
		{
			selectedCharacter = Cast<ABackstabCharacter>(*charactersIt);

			if (currentCharacter != nullptr && currentCharacter == selectedCharacter)
			{
				found = true;
				break;
			}
		}

		characterIteratior.Reset();

		if (currentCharacter != nullptr)
			currentCharacter->ShowPreselection(found);
	}
}


//------------------------------------------------------------------------------------------------
float ABackstabGameMode::GetCameraZoom()
{
	switch (_cameraMode)
	{
		case ECameraMode::CM_Follow:
			return GetCurrentCharacterController()->GetCameraZoom();

		case ECameraMode::CM_Strategy:
			return _strategyCameraController->GetCameraZoom();

		default:
			return 0.0f;
	}
}


//------------------------------------------------------------------------------------------------
void ABackstabGameMode::SetCameraZoom(float zoom)
{
	switch (_cameraMode)
	{
		case ECameraMode::CM_Follow:
			GetCurrentCharacterController()->SetCameraZoom(zoom);
			break;
		
		case ECameraMode::CM_Strategy:
			_strategyCameraController->SetCameraZoom(zoom);
			break;
			
		default:
			break;
	}
}


//------------------------------------------------------------------------------------------------
void ABackstabGameMode::SwitchCameraMode()
{
	switch (_cameraMode)
	{
		case ECameraMode::CM_Follow:
			_strategyCameraController->SetCameraZoom(GetCurrentCharacterController()->GetCameraZoom());
			_strategyCameraController->GetStrategyCameraPawn()->SetActorLocation(GetCurrentCharacterController()->GetBackstabCharacter()->GetActorLocation());
			_strategyCameraController->ClientSetViewTarget(_strategyCameraController->GetStrategyCameraPawn());
			_cameraMode = ECameraMode::CM_Strategy;
			break;
		case ECameraMode::CM_Strategy:
			GetCurrentCharacterController()->SetCameraZoom(_strategyCameraController->GetCameraZoom());
			_strategyCameraController->GetStrategyCameraPawn()->SetActorLocation(GetCurrentCharacterController()->GetBackstabCharacter()->GetActorLocation());
			_strategyCameraController->ClientSetViewTarget(GetCurrentCharacterController()->GetBackstabCharacter());
			_cameraMode = ECameraMode::CM_Follow;
			break;
		default:
			break;
	}
}