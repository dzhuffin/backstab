// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BackstabGameMode.h"
#include "GameFramework/PlayerController.h"
#include "CommonPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class BACKSTAB_API ACommonPlayerController : public APlayerController
{
	GENERATED_BODY()
	

private:


	//------------------------------------------------------------------------------------------------
	ABackstabGameMode *_gameMode;

	FHitResult _mouseMoveHit;
	FHitResult _touchMoveHit;


protected:


	//------------------------------------------------------------------------------------------------
	virtual void SetupInputComponent() override;


	//------------------------------------------------------------------------------------------------
	void OnChangeCharacter();
	void OnSwitchCameraMode();
	void MoveToMouseCursor();
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);


public:


	//------------------------------------------------------------------------------------------------
	void SetNewMoveDestination(const FVector destLocation);


	//------------------------------------------------------------------------------------------------
	FORCEINLINE class ABackstabGameMode* GetGameMode() const { return _gameMode; }
	
	
};
