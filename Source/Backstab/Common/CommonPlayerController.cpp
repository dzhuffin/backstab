// Fill out your copyright notice in the Description page of Project Settings.

#include "Backstab.h"
#include "CommonPlayerController.h"


//------------------------------------------------------------------------------------------------
void ACommonPlayerController::SetupInputComponent()
{
	_gameMode = Cast<ABackstabGameMode>(UGameplayStatics::GetGameMode(this));

	Super::SetupInputComponent();

	InputComponent->ClearActionBindings();

	InputComponent->BindAction("SwitchCameraMode", IE_Pressed, this, &ACommonPlayerController::OnSwitchCameraMode);
	InputComponent->BindAction("ChangeCharacter", IE_Pressed, this, &ACommonPlayerController::OnChangeCharacter);

	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ACommonPlayerController::MoveToTouchLocation);
	InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ACommonPlayerController::MoveToTouchLocation);
}


//------------------------------------------------------------------------------------------------
void ACommonPlayerController::OnSwitchCameraMode()
{
	GetGameMode()->SwitchCameraMode();
}


//------------------------------------------------------------------------------------------------
void ACommonPlayerController::OnChangeCharacter()
{
	GetGameMode()->SwitchCharacter();
}


//------------------------------------------------------------------------------------------------
void ACommonPlayerController::MoveToMouseCursor()
{
	_mouseMoveHit.bBlockingHit = false;

	GetHitResultUnderCursor(ECC_Visibility, false, _mouseMoveHit);

	if (_mouseMoveHit.bBlockingHit)
	{
		SetNewMoveDestination(_mouseMoveHit.ImpactPoint);
	}
}


//------------------------------------------------------------------------------------------------
void ACommonPlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	_touchMoveHit.bBlockingHit = false;
	FVector2D ScreenSpaceLocation(Location);

	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, _touchMoveHit);
	if (_touchMoveHit.bBlockingHit)
	{
		SetNewMoveDestination(_touchMoveHit.ImpactPoint);
	}
}


//------------------------------------------------------------------------------------------------
void ACommonPlayerController::SetNewMoveDestination(const FVector destLocation)
{
	APawn* const Pawn = GetPawn();
	if (Pawn)
	{
		UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
		float const Distance = FVector::Dist(destLocation, Pawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if (NavSys && (Distance > 120.0f))
		{
			NavSys->SimpleMoveToLocation(this, destLocation);
		}
	}
}