#include "Backstab.h"
#include "BackstabCharacter.h"
#include "Animation/BackstabAnimInstance.h"


//------------------------------------------------------------------------------------------------
ABackstabCharacter::ABackstabCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	float const capsuleHeight = 96.0f;

	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, capsuleHeight);

	GetMesh()->RelativeLocation = FVector(0.0f, 0.0f, -88.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	GetCharacterMovement()->GetNavAgentPropertiesRef().bCanCrouch = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->AttachTo(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false;

	SelectorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SelectorMesh"));
	SelectorMesh->AttachTo(RootComponent);
	SelectorMesh->bGenerateOverlapEvents = false;
	SelectorMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SelectorMesh->RelativeLocation = GetMesh()->RelativeLocation;
	SelectorMesh->bAbsoluteRotation = true;
	SelectorMesh->SetCastShadow(false);

	BodyPose = ECharacterPose::CP_Stand;

	IsInMovementLoop = false;
	NonBreakMovementAngle = 45.0f;
}


//------------------------------------------------------------------------------------------------
void ABackstabCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GetMesh() == nullptr)
		return;

	_animInstance = Cast<UBackstabAnimInstance>(GetMesh()->GetAnimInstance());
}


//------------------------------------------------------------------------------------------------
void ABackstabCharacter::OnSelectorChanged(bool selected)
{
	SelectorVisible = selected;
	SelectorMesh->SetVisibility(selected);
}


//------------------------------------------------------------------------------------------------
void ABackstabCharacter::ShowPreselection(bool visible)
{
	GetMesh()->SetRenderCustomDepth(visible);
}


//------------------------------------------------------------------------------------------------
void ABackstabCharacter::ChangePose(ECharacterPose::Pose newPose)
{
	if (BodyPose == newPose)
		return;

	BodyPose = newPose;

	if (BodyPose == ECharacterPose::CP_Sit)
		GetCharacterMovement()->bWantsToCrouch = true;
	else
		GetCharacterMovement()->bWantsToCrouch = false;
}


//------------------------------------------------------------------------------------------------
void ABackstabCharacter::SetAnimSpeed(float newValue)
{
	if (_animInstance != nullptr)
		_animInstance->Speed = newValue;
}

//------------------------------------------------------------------------------------------------
void ABackstabCharacter::SetAnimArchAngle(float newValue)
{
	if (_animInstance != nullptr)
		_animInstance->ArchAngle = newValue;
}

//------------------------------------------------------------------------------------------------
void ABackstabCharacter::StartMovement(float speed)
{
	if (_animInstance != nullptr)
	{
		_animInstance->StartMovementTrigger = true;
		_animInstance->StopMovementTrigger = false;
		_animInstance->Speed = speed;
	}
}

//------------------------------------------------------------------------------------------------
void ABackstabCharacter::BreakMovementForTurn(float turnAngle)
{
	if (_animInstance != nullptr)
	{
		_animInstance->BreakForTurn = true;
		_animInstance->StartAngle = turnAngle;
	}
}

//------------------------------------------------------------------------------------------------
void ABackstabCharacter::StopMovement()
{
	if (_animInstance != nullptr)
	{
		_animInstance->StopMovementTrigger = true;
		_animInstance->StartMovementTrigger = false;
		_animInstance->ArchAngle = 0.0f;
	}
}